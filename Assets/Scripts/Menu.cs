﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour {
	public Animator transicao;
	public Image[] temaBotoes;

	public Tema[] temas;
	public DadosPartida dados;
	public Som som;

	void Start(){
		dados.EscolherTemaAtual(temas[0]);
	}

	//Inicia a transicao
	public void IniciarJogo(){
		transicao.SetTrigger("IniciandoPartida");
	}

	public void CarregarCenaJogo(){
		DontDestroyOnLoad(dados);
		SceneManager.LoadScene(1);
	}

	public void SairJogo(){
		Application.Quit();
	}

	public void AtualizarVolume(double volume){
		som.AtualizarVolume(volume);
		dados.volume = volume;
	}

	public void AtivarDesativarSom(){
		som.AtivarDesativarSom();
		dados.volume = 0.0;
	}

	public void EscolherTema(int idTema){
		dados.EscolherTemaAtual(temas[idTema]);
		MostrarTemaEscolhidoMenu(idTema);
	}

	public void MostrarTemaEscolhidoMenu(int i){
		for(int j =0; j < temaBotoes.Length; j++){
			Color outraCor = temaBotoes[j].color;
			outraCor.a = 1f;
			temaBotoes[j].color = outraCor;
		}
		Color c = temaBotoes[i].color;
		c.a = 0.5f;
		temaBotoes[i].color = c;
	}
}
